﻿using System;
using System.Collections.Generic;

namespace SyntaxysAnalyzator
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "";
            while (str != ":q")
            {
                Console.Clear();
                str = Console.ReadLine();
                DKA(str);
                Console.ReadKey();
            }

        }

        static void DKA(string str)
        {
            Dictionary<string, string> parts = new Dictionary<string, string>();
            parts = new Dictionary<string, string>();

            //int i = 0;
            string subText = "";
            bool isIndeficator = false;

            int lengthStr = str.Length;
            for (int i = 0; i < lengthStr; i++)
            {

                char s = str[i];
                if (Lexema.isNumber(s) && !isIndeficator)
                {
                    subText += s;
                    //Console.WriteLine(subText);
                    if ((i + 2 < lengthStr) && (str[i + 1] == '.' && Lexema.isNumber(str[i + 2])))
                    {
                        subText += str[i + 1];
                        i++;
                    }
                    else if ((i + 3 < lengthStr) && ((str[i + 1] == 'e' || str[i + 1] == 'E')
                        && Lexema.isSigns(str[i + 2]) && Lexema.isNumber(str[i + 3])))
                    {
                        subText += str[i + 1];
                        subText += str[i + 2];
                        i += 2;
                    }
                    else if ((i + 1 == lengthStr) || (!(Lexema.isNumber(str[i + 1]))))
                    {
                        //Console.WriteLine(subText);
                        parts.Add(i.ToString() + " " + subText, "Number");
                        subText = "";
                    }
                }
                else if (Lexema.isOperator(s))
                {
                    parts.Add(i.ToString() + " " + s, "Operator");
                }
                else if (Lexema.isSymbolId(s) || isIndeficator)
                {
                    isIndeficator = true;
                    subText += s;

                    if ((i + 1 == lengthStr) || (!Lexema.isSymbolId(str[i + 1]) && !Lexema.isNumber(str[i + 1])))
                    {
                        isIndeficator = false;
                        parts.Add(i.ToString() + " " + subText, "Id");
                        subText = "";
                    }


                }
                else if (Lexema.isComa(s))
                {
                    parts.Add(i.ToString() + " " + s, "Coma");
                }
                else if (Lexema.isrParen(s))
                {
                    parts.Add(i.ToString() + " " + s, "rParen");
                }
                else if (Lexema.islParen(s))
                {
                    parts.Add(i.ToString() + " " + s, "lParen");
                }

            }

            foreach (KeyValuePair<string, string> pair in parts)
            {
                Console.WriteLine(pair.Key + " " + pair.Value);
            }
        }
    }
}
