﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SyntaxysAnalyzator
{
    class Lexema
    {
        private static char[] operators = new char[] { '+', '*', '/', '^', '-' };
        private static char[] letters = Enumerable.Range('a', 'z' - 'a' + 1).Select(c => (char)c).ToArray();

        public static bool isComa(char symbol)
        {
            return (symbol == ',');
        }

        public static bool isrParen(char symbol)
        {
            return (symbol == ')');
        }

        public static bool islParen(char symbol)
        {
            return (symbol == '(');
        }

        public static bool isOperator(char symbol)
        {
            return (Array.IndexOf(Lexema.operators, symbol) != -1);
                
        }

        public static bool isNumber(char symbol)
        {
            return Char.IsDigit(symbol);
        }

        public static bool containsDotAndE(string str)
        {
            return (str.Contains('.') || str.Contains('e') || str.Contains('E'));
        }

        public static bool isSigns(char symbol)
        {
            return (symbol == '+' || symbol == '-');
        }

        public static bool isSymbolId(char symbol)
        {
            return (Array.IndexOf(Lexema.letters, symbol) != -1 || symbol == '_');
        }
    }
}
